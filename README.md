##### Backend
- API to register and login user.
- API using which login in user can add, edit and update it's product.
- Pagination API to give list of particular user's added product.
- JWT token authentication.
- backend run on port 2000

##### Frontend
- Creating a Component in React and create routing.
- Using react and material UI created form to login user and can register new user.
- Product add, edit, delete and pagination also you can search product by name.
- Making HTTP calls using Axios.
- Calling multipart api with image upload.
- frontend run on port 3000

### Prerequisites
Below noted things you need to install to run this project in your system

- Node.js
- NPM
- MongoDB

### To Setup
Clone or download this repository

1. `cd /backend`
2. `npm install` 
3. `cd /frontend`
4. `npm install` 
### To Run
To run node server
1. `cd /backend`
2. `node server.js`

To run react frontend
1. `cd /frontend`
2. `npm start`


